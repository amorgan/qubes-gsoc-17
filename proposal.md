# Introduction

This proposal aims to provide a detailed overview into the design and implementation of the automatic opening of files based on their path and MIME-type in a secure and configurable manner.

Currently users must use the Qubes-provided context menu actions each time they want to open an untrusted file. This is not only a hindrance to convenience, but a potential security risk if the user happens to forget to right-click.

In order to address these issues, the ability to "mark" a file, folder or an entire file type as untrusted is thus desirable.

To achieve this, a new context menu will be added to all supported file managers (Nautilus, Dolphin) with the string "Always Open in DisposableVM...". When clicked, a dialog box will appear, allowing the user to choose whether to mark that file, folder and/or file type as untrusted. Double-clicking or opening that file in the future should then always result in the spawning of a DisposableVM.

# Project goals

The following steps document the required tasks for complete handling of trust by supported file managers:

1. Implement python context menu in Dolphin and Nautilus as well as minimal GUI for modifying file trust settings
2. Create a patch for Nautilus and Dolphin to follow these settings when opening a file
3. Create system daemon to watch and enforce file trust settings on files created inside of untrusted folders
4. Documentation of implementation
5. Unit testing and integration

## Future Developments

If time permits, the following additional features will be added:

1. Implement the same changes on Windows-based AppVMs
    - Hindrances include the lack of a supported Windows-compatible Extended File Attributes interface for python, Microsoft's own FileSystemWatcher as a replacement for inotify, as well as inexperience with Windows programming

2. Support moving/renaming a untrusted folder
    - The inotify system cannot handle folder moves unless the folder is moved to another watched location. This limitation can be overcome with a kernel module waiting on the rename sys-call, however the implementation's complexity is then much higher

3. Implement a method for the user to tell a file is untrusted at-a-glance, such as emblems or a modified file icon
    - Nautilus still supports emblems, vanilla Dolphin currently does not

# Implementation

## Implement python context menu in Dolphin and Nautilus as well as minimal GUI for modifying file trust settings

  An action menu item will be added for each file manager that leads to a [new dialog window](https://i.imgur.com/zghUUxL.png), allowing the user to modify current and future trust levels for that file or file type.

  The new dialog box allows the user to set a file or file type to always open in a DisposableVM. If the user chooses to mark the file, an extended file attribute [0] will be placed on the file to mark it as untrusted. If the file type is also marked, that file's MIME-type is placed in a list contained within a file under the user's home directory. The file will list all untrusted MIME-types, one per line, and is checked upon opening a file through a supported file manager.

  Additionally, just as the existing "Convert to Trusted PDF" context menu only appears for files, a context menu item only for folders will be added. This will be a simple toggle, allowing the user to mark a folder as untrusted. Once marked, all files and folders existing within that folder will also be considered untrusted.

  To keep track of marked folders, folder paths are added line-by-line to a file in a similar fashion in how MIME-types are handled. This allows a program to have an immediate overview of all untrusted folders without having to search for file attributes recursively.

## Create a patch for Nautilus and Dolphin to follow these settings when opening a file

  Originally the plan was to modify the xdg-open and xdg-mime bash scripts to be aware of untrusted file types. However after some further research, neither Nautilus nor Dolphin utilize these scripts, and instead detect file types on their own.

  As such, they must be patched to open files through `qvm-open-in-dvm` when one of three criteria are met:

  1. The file itself has been marked as untrusted
  2. The file is contained within a directory that has been marked as untrusted
  3. The file type of the file has been marked as untrusted

The patches are designed in a way as to not be Qubes-specific in order to ease the process of upstreaming.

The patches will allow for file manager extensions to be executed upon a file open event. By doing so we apply a more generic approach, which can be useful to other potential projects besides our own.

## Create system daemon to watch and enforce file trust settings on files created inside untrusted folders

  Using the inotify kernel subsystem [1], a daemon will be started on boot that monitors untrusted folders for any files that are created within them. If so, those files are also marked as untrusted, and will carry the attribute even when moved out of that folder. This is to ensure potentially dangerous files are not opened even after some folder restructuring.

  C's inotify API was chosen over python's pyinotify module due to the noticeable speed advantage of a compiled language.

## Documentation of implementation

  Documentation will be completed throughout the course of the project while the code is still fresh in my mind. Polish and finalization will be carried out towards the end.

## Unit testing and integration

  Tests for the following actions will be written:

**Utilities**

  * Appropriate untrusted attributes are set for each GUI option
  * Files created/modified within untrusted folders are set to untrusted automatically

**Nautilus/Dolphin**

  * Opening files through left-click should result in a new DisposableVM if they are untrusted, otherwise should open locally
  * Context/action menu items appear and behave correctly
  * File emblems are correctly displayed (if emblems end up being implemented)

For GUI testing Marek has pointed out the "dogtail" library [2], and it seems it will work for both Nautilus and Dolphin due to its support for both GTK+ and QT-based windows!

# Timeline

My University courses conclude on May 9th, so I will have plenty of free time to get up to speed. Besides the Community Bonding Period which will be spent becoming more familiar with Qubes' internal building tools and participating in community discussions, the official coding portion begins on May 30th. As such, a rough timeline would look like the following:

**May 30th - June 13th** (Two weeks)

Complete the File Manager context menus and file trust modifier GUIs

**June 14th - July 12th** (Four weeks)

Nautilus and Dolphin patch to correctly open files based on their trust level

**July 13th - July 20th** (One week)

Python daemon to monitor untrusted folders and correctly mark contained file contents as untrusted on file events

**July 21st - August 4th** (One week)

Documentation finalization and polish

**August 5th - August 20th** (Two weeks)

Unit test finalization and ensuring smooth integration into the rest of the Qubes ecosystem

**August 21st - August 29th** (One week)

Final project submission and mentor evaluation :)

A weekly formal posting to the qubes-devel mailing list will be carried out including updates on my current progress as well as what tasks still remain. If any of the above goals are completed ahead of schedule I will attempt some of the Future Development items.

Work during the Summer could possibly include odd jobs, but no full or part-time jobs are currently planned. I will be traveling to Florida from August 12th through August 17th. Internet connection would should be available at all hours and I will have my computer to maintain communication through email.

Qubes is the only project I am submitting a proposal to this year.

# About me

I am a student finishing up my second year at the University of California Merced, aiming towards a Masters degree in Computer Science and Engineering. I have worked with and contributed to many open-source projects as viewable on my GitHub profile.

Qubes has been acting as my primary OS since last November and I have thoroughly enjoyed my time with it. I have utilized qubes-builder before for constructing a few templates and have participated in discussions on both the qubes-users and qubes-devel mailing lists, #qubes irc, as well as the /r/Qubes subreddit.

Working with my mentor Marek Marczykowski-Górecki and the rest of the Qubes community would be both a fun and productive use of my time over the Summer, and I hope to then carry on developing for Qubes in my spare time even after the conclusion of the Summer of Code program. Thank you!

# Contact information

Email: me at amorgan.xyz

GitHub: https://github.com/anoadragon453

Website: https://amorgan.xyz

Timezone: PST (GMT-8)

[0]: https://www.freedesktop.org/wiki/CommonExtendedAttributes/
[1]: http://man7.org/linux/man-pages/man7/inotify.7.html
[2]: https://gitlab.com/dogtail/dogtail
